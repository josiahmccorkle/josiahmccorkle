
var express = require("express"),
app = express(),
path = require("path"),
bodyParser = require("body-parser");


// Database
var mongojs = require('mongojs');
var db = mongojs('contactInfo');
var contactColl = db.collection('contact');

app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

db.on('connect', function () {
	console.log('database connected')
})

app.use(express.static(__dirname + "/node_modules"));

// POST method route
app.post('/submitContactInfo/', function (req, res) {
  var contact = req.body;
  console.log(contact);
  contactColl.save(contact, function(err,docs){
    if(err === null){
      res.setHeader('Content-Type', 'application/json');
      res.end();
    } else {
      console.log(err);
      res.send(JSON.stringify(err));
      res.end();  	
    }
  }); 
})

app.get('/', function (req, res) {
  res.redirect("http://localhost:4200");
});

app.listen(1338);
console.log("Server running at port :1338/");