import Ember from 'ember';
import fetch from 'fetch';

export default Ember.Component.extend({
	
	
	actions: {
	    submitContactInfo() {
	    	fetch('http://localhost:1337/submitContactInfo', {
			  method: 'POST',
			  headers: {
			    'Content-Type': 'application/json'
			  },
			  body: JSON.stringify({
			    name: this.get("name") ,
			    email: this.get("email"),
			    phoneNumber: this.get("phoneNumber"),
			    message: this.get("message")
			  })
			}).then(success, error);
	    	Ember.$('#collapse-section').on('shown.bs.collapse', function () {
				  setTimeout(function(){Ember.$('#collapse-section').collapse('toggle')}, 3200);
				});
	    	Ember.$('#collapse-section').collapse('toggle');

		    function success (){
		   		Ember.$("#message").html("<i class=\"fa fa-check\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Thanks! We'll have our people call your people.");	
		    }

		    function error(){
		    	Ember.$("#message").html("<i class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Oops, looks like something went wrong!");
		    }
	    }


  	}
});
